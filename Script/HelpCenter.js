﻿//USEUNIT ImportUnits
/*--------------------Helpcenter page objects--------------------------*/
//var Page_MyPolicy = Aliases.Browser.Page_MyPolicy
//var Textbox_Headerquery = Aliases.Browser.Page_MyPolicy.Header_Main.Textbox_Headerquery
//var Panel_Searchbtn = Aliases.Browser.Page_MyPolicy.Header_Main.Panel_Searchbtn
//var Link_HelpCenter = Aliases.Browser.Page_MyPolicy.Header_Main.Navigate_Desktop.Link_HelpCenter
//var Textbox_Query = Aliases.Browser.Page_MyPolicy.Textbox_Query
//var Button_Search = Aliases.Browser.Page_MyPolicy.Button_Search
//var Textnode_SearchResult = Aliases.Browser.Page_MyPolicy.panelContainer.Txtnode_Namechange
//var Textbox_Firstname = Aliases.Browser.Page_MyPolicy.Textbox_Firstname
//var Textbox_Lastname = Aliases.Browser.Page_MyPolicy.Textbox_Lastname
//var Panel_Continue = Aliases.Browser.Page_MyPolicy.Panel_Continue
//var Textnode_Name_BeforeChange = Aliases.Browser.Page_MyPolicy.panelContainer.panelPreviewbody.Textnode_Name_BeforeChange
//var Textnode_Name_AfterChange = Aliases.Browser.Page_MyPolicy.panelContainer.panelPreviewbody.Textnode_Name_AfterChange
//var Button_SubmitTicket = Aliases.Browser.Page_MyPolicy.panelContainer.Button_SubmitTicket
//var Panel_ConfirmMessage = Aliases.Browser.Page_MyPolicy.panelContainer.Panel_ConfirmMessage
//var Link_Signout = Aliases.Browser.Page_MyPolicy.Header_Main.Link_Signout
//var res=Aliases.Browser.pageQaOnlineservicinginternalOnl.panelAddaccountbtn


/*--------------------Helpcenter page functions--------------------------*/
function Help_Center_NameChange()
{
  Log.AppendFolder("Enter the FAQ")
  Enter_FAQ()  
  Log.PopLogFolder()
  
  Log.AppendFolder("Click on the FAQ")
  Dynamic_Wait(Textnode_SearchResult,2)
  Object_Click(Textnode_SearchResult)
  Log.PopLogFolder()

  Log.AppendFolder("Capture the existing details")
  Dynamic_Wait(Textbox_Firstname,2)
  Firstname = Textbox_Firstname.Text
  Lastname = Textbox_Lastname.Text
  Log.Checkpoint(Firstname)
  Log.Checkpoint(Lastname)
  Log.PopLogFolder()

  Log.AppendFolder("Edit the details")
  Object_Keys(Textbox_Firstname,"Testing")
  Object_Keys(Textbox_Lastname,"Demo")
  Object_Click(Panel_Continue)
  Log.PopLogFolder()

  Log.AppendFolder("Validate the details entered and submit the request")
  Dynamic_Wait(Textnode_Name_BeforeChange,2)
  Log.Message("Details before editing "+Textnode_Name_BeforeChange.contentText)
  if(Textnode_Name_AfterChange.contentText == "Testing"+" "+"Demo")
  {
    Log.Checkpoint("Details have been updated with " +"Testing"+" "+"Demo")
  }
  else
  {
    Log.Message("Details haven't been updated")
  }
  Object_Click(Button_SubmitTicket)
  Log.PopLogFolder()

  Log.AppendFolder("Validate the confirmation message")
  Dynamic_Wait(Panel_ConfirmMessage,3)
  Log.Checkpoint("Success message in the application: "+Panel_ConfirmMessage.contentText)
  Log.PopLogFolder()
   
  Log.AppendFolder("Validate the database details with the expected values")
  Details_To_Validate = BatchEmail_Footprint()
  Template_Fetched = Details_To_Validate[0]
  if(Template_Fetched == "NewUserValidationCode.xslt")
  Log.Checkpoint("Template in the database: "+Template_Fetched+" matches with the expected value")
  SenderEmail_Fetched = Details_To_Validate[1]
  if(SenderEmail_Fetched == "Homesite.Online_Servicing@homesite.com")
  Log.Checkpoint("SenderEmail in the database: "+SenderEmail_Fetched+" matches with the expected value")
  BatchStatus_Fetched = Details_To_Validate[2]
  if(BatchStatus_Fetched == "c")
  Log.Checkpoint("BatchStatus in the database: "+BatchStatus_Fetched+" matches with the expected value")
  Log.PopLogFolder()
}

function Enter_FAQ()
{
  Dynamic_Wait(Textbox_Headerquery,3)
  Object_Keys(Textbox_Headerquery,"Name change")
  Object_Click(Panel_Searchbtn)
}