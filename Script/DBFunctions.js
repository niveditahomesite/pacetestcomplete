﻿//USEUNIT ImportUnits

/*********************************************************************************************************************************************************************************
'
'     NAME                      :   DBFunctions  
'     AUTHOR                    :   Nivedita, Yash 
'     DATE                      :   3/22/2019
'     DESCRIPTION               :   This file consists of database functions 
'
'*********************************************************************************************************************************************************************************/

//DB Server name 
var dbSvcinfo = {
    qa: QA_dbSvcinfo,
    uat: UAT_dbSvcinfo
}
var dbWeb = {
    qa: QA_dbWeb,
    uat: UAT_dbWeb
}
var dbDMBilling = {
    qa: QA_dbDMBilling,
    uat: UAT_dbDMBilling
}
var dbISO = {
    qa: QA_dbISO,
    uat: UAT_dbISO
}


//Execute DB Query and return result based on the column name
function dbExecuteQueryReturnValue(servername, dbname, dbquery, colName) {
    // Create and open a connection to the database
    try {
        var Conn = getActiveXObject("ADODB.Connection");
        Conn.ConnectionString = "Provider=SQLOLEDB.1;Integrated Security=SSPI;Persist Security Info=False;Initial Catalog=" + dbname + ";Data Source=" + servername[env]
        Conn.Open();
        // Create a recordset
        var Rs = getActiveXObject("ADODB.Recordset");
        // Open the recordset
        queryresult = Rs.Open(dbquery, Conn);
        // Retrieves the results based on the column names
        Rs.MoveFirst();
        while (!Rs.EOF) {
            result = Rs.Fields.Item(colName).Value;
            Rs.MoveNext();
        }
        // Close the recordset and connection
        Rs.Close();
        Conn.Close();
        //Log.Message(result)
        //Log.Message("SQL query executed server name- " + servername[env] + " DB Table- " + dbname + " query- " + dbquery + " dbColumn- " + colName)
        //Return result   
        return result
    } catch (e) {
        //log error if unable to connect to db or sql returns no result 
        Log.Error("SQL query does not return any value server name- " + servername[env] + " DB Table- " + dbname + " query- " + dbquery + " dbColumn- " + colName)
        //exit(0)
    }
}


//function returns policy numb, brand,amf account number ,form, state, zip, dob, lname, password, email, ssoEnroll, effDt 
//parameter - Policy Number 
function getPolicyInfo(pnumb) {
    try {

        //concatenate policy number with the brand query and get the policy brand information from branding table 
        dbPolicyInfo = dbGetPolicyInfo + pnumb
        //get brand information from HS Service Info table 
        var brand = dbExecuteQueryReturnValue(dbISO, "ISO_PROD", dbPolicyInfo, "BRAND")
        //get form information from HS Service Info table
        var form = dbExecuteQueryReturnValue(dbISO, "ISO_PROD", dbPolicyInfo, "FormNumber")
        //get amf account information from HS Service Info table
        var amf = dbExecuteQueryReturnValue(dbISO, "ISO_PROD", dbPolicyInfo, "amf_account_number")
        //get state information from HS Service Info table
        var state = dbExecuteQueryReturnValue(dbISO, "ISO_PROD", dbPolicyInfo, "address_line3_state")
        //get zip code information from HS Service Info table
        var zip = dbExecuteQueryReturnValue(dbISO, "ISO_PROD", dbPolicyInfo, "zip_code1")
        //get dob from HS Service Info table
        var dob = dbExecuteQueryReturnValue(dbISO, "ISO_PROD", dbPolicyInfo, "date_of_birth")
        //get last name information from HS Service Info table
        var lname = dbExecuteQueryReturnValue(dbISO, "ISO_PROD", dbPolicyInfo, "last_name_of_customer")
        //get policy start date information from HS Service Info table
        var effDt = dbExecuteQueryReturnValue(dbISO, "ISO_PROD", dbPolicyInfo, "rate_guarantee_date")
          //concatenate policy number with enrollment table and get the username and password for the policy from enrollment table 
          //policyEnrolledStatus = dbGetEnrollmentStatus + pnumb
          //get password from PolicyServices 
          //var password = dbExecuteQueryReturnValue(dbWeb, "PolicyServices", policyEnrolledStatus, "password")
          //get email from PolicyServices 
          //var email = dbExecuteQueryReturnValue(dbWeb, "PolicyServices", policyEnrolledStatus, "email")
          //get ssoEnrollment status from PolicyServices 
          //var ssoEnroll = dbExecuteQueryReturnValue(dbWeb, "PolicyServices", policyEnrolledStatus, "PersistentNameId")
        //concatenate policy number with enrollment table and get the enrollment and username
        policyEnrolledQ = dbGetPolicyEnrolled + pnumb
        //get enrollment status 
        var enroll = dbExecuteQueryReturnValue(dbWeb, "PolicyServices", policyEnrolledQ, "PolicyNumber")
        //get email address 
        var email = dbExecuteQueryReturnValue(dbWeb, "PolicyServices", policyEnrolledQ, "email_address")
                 
        //array Result for passing the policy numner, brand, password and user id (email address)
        var Result = []
        Result.push(pnumb, brand, amf, form, state, zip, dob, lname, effDt, enroll, email)
        Log.Message(Result)
        //Return result
        return Result
    } catch (e) {
        //log error if unable to connect to db or sql returns no result 
        Log.Error("SQL query issue - Unable to fetch policy information for Policy - " + pnumb)
        //exit(0)
    }
}

// function returns the brand email address for the policy number entered 
function getPartnerEmailAddres(pnumb)
{
  //Brand email address query concatenated with policy number and '
  dbBatchEmailPartnerInfoQ = dbBatchEmailPartnerInfo+pnumb+"'"
  //Get brandEmailAddress from SelfServiceEmail from Batch Partner Info table 
  var brandEmailAddress = dbExecuteQueryReturnValue(dbISO, "ISO_PROD", dbBatchEmailPartnerInfoQ, "SelfServiceEmail")
  //Return brand email address based on the partner ID 
  return brandEmailAddress
  
}

//function returns the brand phone number from brand table 
function getBrandPhoneNumb(pnumb)
{
  //Brand phone number query concatenates with policy number 
  dbGetPolicyBrandQ = dbGetPolicyBrand+pnumb
  //Get brand phone number from brand table 
  var brandPhoneNumb = dbExecuteQueryReturnValue(dbISO, "ISO_PROD", dbGetPolicyBrandQ, "CO_PHONE_NUMBER")
  //Return brand Phone number based on the policy number 
  return brandPhoneNumb
}

//function returns form type from hs service info table 
function getForm(pnumb)
{
  //concatenate policy number with the brand query and get the policy brand information from branding table 
  dbPolicyInfo = dbGetPolicyInfo + pnumb
  //get form information from HS Service Info table
  var form = dbExecuteQueryReturnValue(dbISO, "ISO_PROD", dbPolicyInfo, "FormNumber")
  //Log.Message("form --"+form)
  switch (form) {
        case 2:
            //Homeowner
            formName = "Homeowner"
            break;
        case 3:
            //Homeowner
            formName = "Homeowner"
            break;
        case 4:
            //Renter
            formName = "Renter"
            break;
        case 6:
            //Condo
            formName = "Condo"
            break;
        case 9:
             //Second Home - Homeowner
            formName = "Homeowner"
            break;
        default:
            Log.Error("Invalid form number for policy "+pnumb)
    }
    //Log.Message("formName --"+formName)
    return formName
}

//function returns policy holder email address 
function getEmail(pnumb)
{
         //concatenate policy number with enrollment table and get the enrollment and username
        policyEnrolledQ = dbGetPolicyEnrolled + pnumb
        //get email address 
        var email = dbExecuteQueryReturnValue(dbWeb, "PolicyServices", policyEnrolledQ, "email_address")
        //return policy holder email address 
        return email  
}

//get password and email
//parameter - Policy Number
function getOLSEnrollmentInfo(pnumb) {
    //Concatenate the query with the policy number 
    policyEnrolledStatus = dbGetEnrollmentStatus + pnumb
    // get password 
    var password = dbExecuteQueryReturnValue(dbWeb, "PolicyServices", policyEnrolledStatus, "password")
    // get email address 
    var email = dbExecuteQueryReturnValue(dbWeb, "PolicyServices", policyEnrolledStatus, "email")
    //array Result for passing the  password and user id (email address)
    var Result = []
    Result.push(password, email)
    Log.Message(Result)
    //Return result
    return Result
}

//get random policy number from PaceServiceLog
function getRandomPolicyPaceServiceLog() {

    try {
        //get policy number from pace service log 
        polnumb = dbExecuteQueryReturnValue(dbSvcinfo, "HS_SERVICE_INFO", dbGetPolicyNumber, "PolicyNumber")
        return polnumb
    } catch (e) {
        //log error if unable to connect to db or sql returns no result 
        Log.Error("SQL query issue - Unable to fetch policy from HS_SERVICE_INFO table using query " + dbGetPolicyNumber)

    }
}

//get random policy number from HSServiceInfo table 
function getRandomPolicyHSService() {
    try {
        //get policy number from HS Client Search table  
        polnumb = dbExecuteQueryReturnValue(dbISO, "ISO_PROD", dbGetPolicyNumberHS, "POLICY_NUMBER")
        return polnumb
    } catch (e) {
        //log error if unable to connect to db or sql returns no result 
        Log.Error("SQL query issue - Unable to fetch policy from ISO_PROD table using query " + dbGetPolicyNumberHS)

    }
}

//get validation code from batch email message queue table 
//parameter - Policy number 
function getValidationCodeBatch(polnumb) {

    try {
        //concatenate the policy number with the query  
        var dbGetValidationCodeQ = dbGetValidationCode+polnumb+dborderbymessage
        valCode = dbExecuteQueryReturnValue(dbISO, "ISO_PROD", dbGetValidationCodeQ, "Custom03")
        return valCode
    } catch (e) {
        //log error if unable to connect to db or sql returns no result 
        Log.Error("SQL query issue - Unable to fetch validation code using " + dbGetValidationCodeQ + " for policy "+polnumb)

    }
}

//get random policy number from HSServiceInfo table 
//parameter - brand 
function getPolicyPartner(brand) {
    try {
        //get policy number from HS Client Search table based on the brand 
        var dbGetPolicyNumberBrandQ = dbGetPolicyNumberBrand + brand + dborderbyString
        polnumb = dbExecuteQueryReturnValue(dbISO, "ISO_PROD", dbGetPolicyNumberBrandQ, "POLICY_NUMBER")
        return polnumb
    } catch (e) {
        //log error if unable to connect to db or sql returns no result 
        Log.Error("SQL query issue - Unable to fetch policy for Brand - " + brand)

    }
}

//get random policy number from HSServiceInfo table 
//parameter - form 
function getPolicyForm(form) {
    try {
        //get policy number from HS Client Search table based on form 
        var dbGetPolicyNumberFormQ = dbGetPolicyNumberForm + form + dborderbyInt
        polnumb = dbExecuteQueryReturnValue(dbISO, "ISO_PROD", dbGetPolicyNumberFormQ, "POLICY_NUMBER")
        return polnumb
    } catch (e) {
        //log error if unable to connect to db or sql returns no result 
        Log.Error("SQL query issue - Unable to fetch policy for form - " + form)

    }
}

//get random policy number from HSServiceInfo table 
//parameter - amf 
function getPolicyAmf(amf) {
    try {
        //get policy number from HS Client Search table based on Amf number  
        var dbGetPolicyNumberAmfQ = dbGetPolicyNumberAmf + amf + dborderbyIn
        polnumb = dbExecuteQueryReturnValue(dbISO, "ISO_PROD", dbGetPolicyNumberAmfQ, "POLICY_NUMBER")
        return polnumb
    } catch (e) {
        //log error if unable to connect to db or sql returns no result 
        Log.Error("SQL query issue - Unable to fetch policy for amf - " + amf)

    }
}

//get random policy number from HSServiceInfo table 
//parameter - brand and form  
function getPolicyBrandForm(brand, form) {
    try {
        //get policy number from HS Client Search table based on brand and form  
        var dbGetPolicyNumberBrandFormQ = dbGetPolicyNumberBrand + brand + dbGetPolicyNumberFormString + form + dborderbyInt
        polnumb = dbExecuteQueryReturnValue(dbISO, "ISO_PROD", dbGetPolicyNumberBrandFormQ, "POLICY_NUMBER")
        return polnumb
    } catch (e) {
        //log error if unable to connect to db or sql returns no result 
        Log.Error("SQL query issue - Unable to fetch policy for brand - " + brand + " form - " + form)

    }
}

//get random policy number from HSServiceInfo table 
//parameter - amf and form
function getPolicyAmfForm(Amf, form) {
    try {
        //get policy number from HS Client Search table based on Amf number and form 
        var dbGetPolicyNumberAmfFormQ = dbGetPolicyNumberAmf + Amf + dbGetPolicyNumberFormIn + form + dborderbyInt
        polnumb = dbExecuteQueryReturnValue(dbISO, "ISO_PROD", dbGetPolicyNumberAmfFormQ, "POLICY_NUMBER")
        return polnumb
    } catch (e) {
        //log error if unable to connect to db or sql returns no result 
        Log.Error("SQL query issue - Unable to fetch policy for Amf - " + Amf + " form - " + form)

    }
}

//update password in Policy Service table  
//parameter - Policy Number  
function dbUpdatePwd(pnumb) {
    try {
        //get policy number from HS Client Search table based on Amf number and form 
        var dbUpdatePasswordQ = dbUpdatePassword + pnumb + ")"
        polnumb = dbExecuteQuery(dbWeb, "PolicyServices", dbUpdatePasswordQ)
    } catch (e) {
        //log error if unable to connect to db or sql returns no result 
        Log.Error("SQL query issue - Unable to update password -" + pnumb)

    }
}

//Execute DB Query 
function dbExecuteQuery(servername, dbname, dbquery) {
  try {
    var Qry;
    // Create a query
    Qry = ADO.CreateADOQuery();
    // Specify the connection string
    Qry.ConnectionString = "Provider=SQLOLEDB.1;Integrated Security=SSPI;Persist Security Info=False;Initial Catalog=" + dbname + ";Data Source=" + servername[env]
    // Specify the SQL expression
    Qry.SQL = dbquery
    // Execute the query
    Qry.ExecSQL();
    // Close the query
    Qry.Close();
    Log.Message("SQL query executed server name- " + servername[env] + " DB Table- " + dbname + " query- " + dbquery)
    } catch (e) {
        //log error if unable to connect to db or sql returns no result 
        Log.error("SQL query does not return any value server name- " + servername[env] + " DB Table- " + dbname + " query- " + dbquery )

    }
}