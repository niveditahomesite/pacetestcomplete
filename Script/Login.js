﻿//USEUNIT ImportUnits

/*********************************************************************************************************************************************************************************
'
'     NAME                      :   Login  
'     AUTHOR                    :   Nivedita, Yash 
'     DATE                      :   3/22/2019
'     DESCRIPTION               :   This file consists of common functions and objects for Login, Enrollment and Logout for all users 
'
'*********************************************************************************************************************************************************************************/

//---------------------------Login, Enrollment and Logout objects------------------------------------------
//------------------------------SSO user----------------------------------
var txtSSOPN = Aliases.Browser.pgSSOLogin.txtSSOPN
var txtSSOPN1 = Aliases.Browser.pgSSOLogin.txtSSOPN1
var btnSSOPostpreflight = Aliases.Browser.pgSSOLogin.btnSSOPostpreflight
var btnSSOGenSAML = Aliases.Browser.pgSSOLogin.btnSSOGenSAML
var btnSSOPostSAML = Aliases.Browser.pgSSOLogin.btnSSOPostSAML
var txtSSOLN = Aliases.Browser.pgSSOEnroll.artclevalidation.txtSSOLN
var txtSSODOB = Aliases.Browser.pgSSOEnroll.artclevalidation.txtSSODOB
var txtSSOZipcode = Aliases.Browser.pgSSOEnroll.artclevalidation.txtSSOZipcode
var pnlSSOVerifyinfo = Aliases.Browser.pgSSOEnroll.artclevalidation.pnlSSOVerifyinfo
var btnSSOOk = Aliases.Browser.pgSSOEnroll.Codealert.btnSSOOk
var txtSSOEmail = Aliases.Browser.pgSSOEnroll.artclevalidation.txtSSOEmail
var txtSSOValidationcode = Aliases.Browser.pgSSOEnroll.artclevalidation.txtSSOValidationcode
var pnlSSOSendvalcode = Aliases.Browser.pgSSOEnroll.artclevalidation.pnlSSOSendvalcode
var txtSSOCdhid = Aliases.Browser.pgSSOLogin.txtSSOCdhid
var txtSSOExpid = Aliases.Browser.pgSSOLogin.txtSSOExpid
//-----------------------------CAT user-----------------------------------
var txtCATUN = Aliases.Browser.pgCATLogin.txtCATUN
var txtCATPwd = Aliases.Browser.pgCATLogin.txtCATPwd
var pnlCATSignin = Aliases.Browser.pgCATLogin.pnlCATSignin
var lnkCATLogout = Aliases.Browser.pgCATPolicysearch.pnlCATPolicysearch.lnkCATLogout
var txtCATPolicySearch = Aliases.Browser.pgCATPolicysearch.pnlCATPolicysearch.txtCATPolicySearch
var btnCATSearch = Aliases.Browser.pgCATPolicysearch.pnlCATPolicysearch.btnCATSearch
var lnkPolicySearchResult = Aliases.Browser.pgCATPolicysearch.pnlCATPolicysearch.tableResultstable.lnkPolicySearchResult
//-----------------------------SuperUser ---------------------------------
var txtSUUN = Aliases.Browser.pgSULogin.txtSUUN
var txtSUPwd = Aliases.Browser.pgSULogin.txtSUPwd
var btnSULogin = Aliases.Browser.pgSULogin.btnSULogin
var lnkGoBackSU = Aliases.Browser.Header_All.header.lnkGoBackSU
var imgSULogout = Aliases.Browser.pgSULogin.imgSULogout
var txtSUPN = Aliases.Browser.pgSULogin.txtSUPN
var txtnodeSUCustomerstatus = Aliases.Browser.pgSULogin.txtnodeSUCustomerstatus
var btnSUViewPolicy = Aliases.Browser.pgSULogin.btnSUViewPolicy
//-------------------------------OLS--------------------------------------
var txtOLSUN = Aliases.Browser.pgOLSLogin.txtOLSUN
var txtOLSPwd = Aliases.Browser.pgOLSLogin.txtOLSPwd
var btnOLSLogin = Aliases.Browser.pgOLSLogin.btnOLSLogin
var txtnodeOLSCreateaccount = Aliases.Browser.pgOLSLogin.txtnodeOLSCreateaccount
var txtOLSLN = Aliases.Browser.pgOLSLogin.txtOLSLN
var txtOLSDOB = Aliases.Browser.pgOLSLogin.txtOLSDOB
var txtOLSZipcode = Aliases.Browser.pgOLSLogin.txtOLSZipcode
var txtOLSPN = Aliases.Browser.pgOLSLogin.txtOLSPN
var pnlOLSVerifyinfo = Aliases.Browser.pgOLSLogin.pnlOLSVerifyinfo
var txtOLSEmail = Aliases.Browser.pgOLSLogin.txtOLSEmail
var txtOLSNewpwd = Aliases.Browser.pgOLSLogin.txtOLSNewpwd
var pnlOLSCreateaccount = Aliases.Browser.pgOLSLogin.pnlOLSCreateaccount

//var Txtnode_OLS_CreateYourAccount = Aliases.Browser.Pg_OLS_Login.Txtnode_OLS_CreateYourAccount
//var Txtbox_OLS_Lastname = Aliases.Browser.Pg_OLS_Login.Txtbox_OLS_Lastname
//var Txtbox_OLS_Dateofbirth = Aliases.Browser.Pg_OLS_Login.Txtbox_OLS_Dateofbirth
//var Txtbox_Zipcode1 = Aliases.Browser.Pg_OLS_Login.Txtbox_OLS_Zipcode1
//var Txtbox_OLS_Policynumber = Aliases.Browser.Pg_OLS_Login.Txtbox_OLS_Policynumber
//var Pnl_OLS_Verifyyourid = Aliases.Browser.Pg_OLS_Login.Pnl_OLS_Verifyyourid
//var Txtbox_OLS_Userid = Aliases.Browser.Pg_OLS_Login.Txtbox_OLS_Userid
//var Txtbox_OLS_Password = Aliases.Browser.Pg_OLS_Login.Txtbox_OLS_Password

//---------------------------Login, Enrollment and Logout functions------------------------------------------

//login to superuser, click on policy search and navigate to the policy
function loginSU(Environment, pnumb) {
    try {
        var Username = {
            qa: QA_SU_username,
            uat: QA_SU_username
        }
        var Password = {
            qa: QA_SU_pwd,
            uat: QA_SU_pwd
        }
        //validate if username test box shows up 
        dynamicWait(txtSUUN, 2)
        if (txtSUUN.Exists) {
            //enter username     
            objectKeys(txtSUUN, Username[Environment])
            //Enter password  
            objectKeys(txtSUPwd, Password[Environment])
            //Click on login button
            objectClick(btnSULogin)
            //Wait for policy search button 
            dynamicWait(txtSUPN, 20)
            if (txtSUPN.Exists) {
                //Enter the policy number     
                objectKeys(txtSUPN, pnumb)
                //Click on enter 
                objectKeys(txtSUPN, "[Enter]")
                //wait for customer status to show up 
                dynamicWait(txtnodeSUCustomerstatus, 20)
                //Click on view policy 
                objectClick(btnSUViewPolicy)
                //login to the policy and validate if billign link shows up
                dynamicWait(lnkBilling, 3)
                if (lnkBilling.Exists) {
                    Log.Message("Superuser login successful for policy number " + pnumb)
                } else {
                    Log.Error("Superuser login failed for policy number " + pnumb)
                }
            } else {
                Log.Error("Unable to navigate to Superuser policy search page using username " + Username[Environment]);
                return false;
            }
        } else {
            Log.Error("Unable to navigate to Superuser page")
        }
    } catch (e) {
        Log.Error("Superuser login and policy search failed");
    }
}

//Logout from superuser 
function logoutSU() {
    //logout from the ols policy - back to superuser 
    dynamicWait(lnkGoBackSU, 30)
    objectClick(lnkGoBackSU)
    //logout from superuser 
    dynamicWait(imgSULogout, 30)
    objectClick(imgSULogout)
    aqUtils.Delay(2000)
}

//Enrolling in OLS 
function enrollOLS(pnumb) {
    try {
        //Get policy information psnap  
        Xml_values = TestXMLDOM(pnumb, psnap, "PolicyNumber", "ZipCode1", "LastNameOfCustomer", "DateOfBirth01")
        //
        dynamicWait(txtnodeOLSCreateaccount, 2)
        if (txtnodeOLSCreateaccount.Exist) {
            //Click on create account button 
            objectClick(txtnodeOLSCreateaccount)
            //Wait for Last name txt box in enrollment page 
            dynamicWait(txtOLSLN, 2)
            if (txtOLSLN.Exist) {
                //Enter last name     
                objectKeys(txtOLSLN, Xml_values[2])
                //Enter DOB
                objectKeys(txtOLSDOB, aqConvert.DateTimeToFormatStr(Xml_values[3].substring(0, 10), "%m/%d/%Y"))
                //Enter zipcode 
                objectKeys(txtOLSZipcode, Xml_values[1])
                //Enter policy number 
                objectKeys(txtOLSPN, Xml_values[0])
                //Click on Verify Info button 
                objectClick(pnlOLSVerifyinfo)
                //wait for email address text box 
                dynamicWait(txtOLSEmail, 2)
                if (txtOLSEmail.Exists) {
                    //Enter email address concatenated with gmail address      
                    objectKeys(txtOLSEmail, "nivedita.qa+" + pnumb + "@gmail.com")
                    //Enter default password from global variables 
                    objectKeys(txtOLSNewpwd, Password_default)
                    //Click on create account button 
                    objectClick(pnlOLSCreateaccount)
                    //Validate if billing page shows up
                    dynamicWait(lnkBilling, 3)
                    if (lnkBilling.Exists) {
                        Log.Message("OLS enrollment succesful for policy number " + pnumb + " Last name " + Xml_values[2] + " Zipcode " + Xml_values[1] + " DOB " + aqConvert.DateTimeToFormatStr(Xml_values[3].substring(0, 10), "%m/%d/%Y"))
                    } else {
                        Log.Error("OLS enrollment failed for policy number " + pnumb + " Last name " + Xml_values[2] + " Zipcode " + Xml_values[1] + " DOB " + aqConvert.DateTimeToFormatStr(Xml_values[3].substring(0, 10), "%m/%d/%Y"))
                    }
                } else {
                    Log.Error("Navigation to Enrollment email address page failed")
                }
            } else {
                Log.Error("Navigation to Enrollment page failed")
            }
        } else {
            Log.Error("Unable to navigate to OLS login page")
        }
    } catch (e) {
        Log.Message("Enrollment failed for policy number " + pnumb);
    }
}

//login to OLS using policy number and username 
function loginOLS(pnumb, Username) {
    try {
        //Update password in db to default password 
        dbUpdatePwd(pnumb)
        dynamicWait(txtOLSUN, 3)
        if (txtOLSUN.Exists) {
            //Enter username in the username field     
            objectKeys(txtOLSUN, Username)
            //Enter default password in the password field  
            objectKeys(txtOLSPwd, Password_default)
            //Click on Login button 
            objectClick(btnOLSLogin)
            //Validate billign button in the login screen 
            dynamicWait(lnkBilling, 3)
            if (lnkBilling.Exists) {
                Log.Message("OLS login successful for policy number " + pnumb + " Username " + Username)
            } else {
                Log.Error("OLS login unsuccessful for policy number " + pnumb + " Username " + Username)
                return true
            }
        } else {
            Log.Error("Couldn't navigate to the application page took too long to load");
            return false;
        }
    } catch (e) {
        Log.Message("Login failed for policy number " + pnumb + " Username " + Username)
    }

}

//logout of cat user 
function logoutCAT() {
    //Wait and click on back to Cat user search page 
    dynamicWait(lnkGoBackSU, 3)
    objectClick(lnkGoBackSU)
    //wait and clickout of cat user logout button 
    dynamicWait(lnkCATLogout, 3)
    objectClick(lnkCATLogout)
    aqUtils.Delay(2000)
}

//Login to cat user and search for a policy and login
function loginCAT(Environment, policyNumber) {
    try {
        var Username = {
            qa: QA_CAT_username,
            uat: UAT_CAT_username
        }
        var Password = {
            qa: QA_CAT_pwd,
            uat: UAT_CAT_pwd
        }
        //wait for username test box in cat login page 
        dynamicWait(txtCATUN, 2)
        if (txtCATUN.Exists) {
            //enter cat username       
            objectKeys(txtCATUN, Username[Environment])
            //enter cat password
            objectKeys(txtCATPwd, Password[Environment])
            //click on sign in for cat user
            objectClick(pnlCATSignin)
            //wait for policy search to show up 
            dynamicWait(txtCATPolicySearch, 30)
            if (txtCATPolicySearch.Exists) {
                //enter policy number in the policy search text box                         
                objectKeys(txtCATPolicySearch, policyNumber)
                //click on policy search button
                objectClick(btnCATSearch)
                //wait for policy search result 
                dynamicWait(lnkPolicySearchResult, 3)
                if (lnkPolicySearchResult.Exists) {
                    //click on the policy number in the search result 
                    objectClick(lnkPolicySearchResult)
                    //wait for policy page to load
                    dynamicWait(lnkCATLogout, 30)
                    //wait for policy sign out button 
                    if (lnkCATLogout.Exists) {
                        Log.Message("CAT user login successful for policy " + policyNumber)
                        return true
                    } else {
                        Log.Error("CAT user login failed for policy " + policyNumber)
                    }
                } else {
                    Log.Error("CAT user policy search does not return any search result for policy " + policyNumber)
                }
            } else {
                Log.Error("CAT user login failed for username " + Username[Environment] + " password " + Password[Environment])
            }
        } else {
            Log.Error("CAT user login page does not show up");
            return false;
        }
    } catch (e) {
        Log.Error("CAT user login failed for policy " + policyNumber);
    }
}

//Login to Geico policy using SSO 
function loginSSOGCO(Policy) {
    try {
        SSOGCOEnrolled(Policy)
        //if challenge page is displayed then enroll the policy to SSO
        if (!lnkBilling.Exists) {     
          SSOGCOOLSEnrolled(Policy)
                }
                else {  
                  SSOGCONotOLSEnrolled(Policy)
                } 
        }              
        catch (e) {
            Log.Error("Couldn't navigate to the application as page took too long to load")
        }
    }


function loginSSOPGR(Policy) {
    try {
        //wait for policy number field
        dynamicWait(txtSSOPN, 3)
        //enter policy number in policy number field
        objectKeys(txtSSOPN, Policy)
        //click on Postpreflight button
        objectClick(btnSSOPostpreflight)
        //wait for policy number 1 field
        dynamicWait(txtSSOPN1, 3)
        //enter policy number in policy number 1 field
        objectKeys(txtSSOPN1, Policy)
        //wait for generate saml button
        dynamicWait(btnSSOGenSAML, 10)
        //click on generate saml button
        objectClick(btnSSOGenSAML)
        //wait for post saml button
        dynamicWait(btnSSOPostSAML, 10)
        //click on post saml button
        btnSSOPostSAML.RefreshMappingInfo()
        objectClick(btnSSOPostSAML)
        //wait for lastname field
        dynamicWait(txtSSOLN, 3)
        //check for billing link
        if (lnkBilling.Exists) {
            Log.message("Policy " + Policy + " is already SSO enrolled")
        } else {
            //fetch data from psnap
            xml_values = TestXMLDOM(Policy, psnap, "PolicyNumber", "LastNameOfCustomer", "DateOfBirth01", "ZipCode1")
            //enter last name in last name field
            objectKeys(txtSSOLN, xml_values[1])
            //enter dob in dob field
            objectKeys(txtSSODOB, aqConvert.DateTimeToFormatStr(xml_values[2].substring(0, 10), "%m/%d/%Y"))
            //enter zipcode in zipcode field
            objectKeys(txtSSOZipcode, xml_values[3])
            //click on verify information button
            objectClick(pnlSSOVerifyinfo)
            //click on verify information button
            objectClick(pnlSSOVerifyinfo)
            //wait for email field
            dynamicWait(txtSSOEmail, 3)
            //if email field doesn't exist enrollment is complete or else continue enrollment by verifying validation code
            if (lnkBilling.Exists) {
                //wait for billing link     
                dynamicWait(lnkBilling, 10)
                Log.Message("SSO enrollment successful")
            } else {
                Log.Message("Policy not OLS enrolled")
                //enter email in email field
                objectKeys(txtSSOEmail, "nivedita.qa+" + Policy + "@gmail.com")
                //click on send validation code button
                objectClick(pnlSSOSendvalcode)
                //wait for alert pop pup
                dynamicWait(btnSSOOk, 3)
                //click on ok button in alert pop up
                objectClick(btnSSOOk)
                //wait for validaiton code field to be enabled
                dynamicWait(txtSSOValidationcode, 3)
                //retrieve validation code from batch email queue table
                var resultbatch = getValidationCodeBatch(Policy)
                //enter validation code in code field
                objectKeys(txtSSOValidationcode, resultbatch)
                //click on verify information button
                objectClick(pnlSSOVerifyinfo)
                //wait for billing link
                dynamicWait(lnkBilling, 3)
                Log.Message("SSO enrollment successful")
            }
        }
    } catch (e) {
        Log.Error("Couldn't navigate to the application as page took too long to load")
    }

}

function loginSSOAMFM(Policy) {
  try {  
    //wait for Policy number field
    dynamicWait(txtSSOPN, 2)
    //enter policy number in policy number field
    objectKeys(txtSSOPN, Policy)
    //enter cdh id for uwaa policy
    objectKeys(txtSSOCdhid, "1634718")
    //enter expid for uwaa policy
    objectKeys(txtSSOExpid, "7000")
    //click on generate saml button
    objectClick(btnSSOGenSAML)
    //wait for post saml button
    dynamicWait(btnSSOPostSAML, 2)
    //click on post saml button
    objectClick(btnSSOPostSAML)
    //wait for last name field
    dynamicWait(txtSSOLN, 2)
    if (lnkBilling.Exists) {
        Log.message("Policy " + Policy + " is already SSO enrolled")
    } else {
        //fetch data from psnap
        xml_values = TestXMLDOM(Policy, psnap, "PolicyNumber", "LastNameOfCustomer", "DateOfBirth01", "ZipCode1")
        //enter last name in last name field
        objectKeys(txtSSOLN, xml_values[1])
        //enter dob in dob field
        objectKeys(txtSSODOB, aqConvert.DateTimeToFormatStr(xml_values[2].substring(0, 10), "%m/%d/%Y"))
        //enter zipcode in zipcode field
        objectKeys(txtSSOZipcode, xml_values[3])
        //click on verify information button
        objectClick(pnlSSOVerifyinfo)
        //wait for email field
        dynamicWait(txtSSOEmail, 3)
        //if email field doesn't exist enrollment is complete or else continue enrollment by verifying validation code
        if (txtSSOEmail.Exists) {
           Log.Message("Policy not OLS enrolled")
            //enter email in email field
            objectKeys(txtSSOEmail, "nivedita.qa+" + Policy + "@gmail.com")
            //click on send validation code button
            objectClick(pnlSSOSendvalcode)
            //wait for alert pop pup
            dynamicWait(btnSSOOk, 6)
            //click on ok button in alert pop up
            objectClick(btnSSOOk)
            //wait for validaiton code field to be enabled
            dynamicWait(txtSSOValidationcode, 3)
            //retrieve validation code from batch email queue table
            var resultbatch = getValidationCodeBatch(Policy)
            //enter validation code in code field
            objectKeys(txtSSOValidationcode, resultbatch)
            //click on verify information button
            objectClick(pnlSSOVerifyinfo)
            //wait for billing link
            dynamicWait(lnkBilling, 3)
            Log.Message("SSO enrollment successful") 
        } 
    }
} catch (e) {
    Log.Error("Couldn't navigate to the application as page took too long to load")
}
}


//Click on Logout button for OLS user and Go Back button for SSO
function logout() {
    //Click on logout 
    dynamicWait(lnkGoBackSU, 4)
    objectClick(lnkGoBackSU)
}


function SSOGCOEnrolled(Policy)
{
  dynamicWait(txtSSOPN, 2)
  //enter policy number
  objectKeys(txtSSOPN, Policy)
  //click on GenerateSAML button
  objectClick(btnSSOGenSAML)
  //click on PostSAML button
  objectClick(btnSSOPostSAML)
  if (lnkBilling.Exists){
    Log.Message("Policy " + Policy + " is already SSO enrolled")
  }
  else{
    Log.Message("Policy " + Policy + " is not SSO enrolled")
  }
}

function SSOGCOOLSEnrolled(Policy)
{
   Log.Message("Policy " + Policy + " is not SSO enrolled")
  //retrieves data from psnap  
  xml_values = TestXMLDOM(Policy, psnap, "PolicyNumber", "LastNameOfCustomer", "DateOfBirth01", "ZipCode1")
  aqUtils.Delay(100)
  //enter last name fetched from psnap
  objectKeys(txtSSOLN, xml_values[1])
  //enter dob fetched from psnap converting from yyyy-mm-dd to mm/dd/yyyy format
  objectKeys(txtSSODOB, aqConvert.DateTimeToFormatStr(xml_values[2].substring(0, 10), "%m/%d/%Y"))
  //enter zipcode fetched from psnap
  objectKeys(txtSSOZipcode, xml_values[3])
  if (!txtSSOEmail.Exists)
               {            
                Log.Message("Policy " + Policy + " is not SSO enrolled but OLS enrolled")
                //click on verify information
                objectClick(pnlSSOVerifyinfo)
                //wait for billing link in billing page
                dynamicWait(lnkBilling, 3)
                Log.Message("SSO enrollment for Policy " + Policy + " is successful")   
                }
  else{
    Log.Message("Policy " + Policy + "is not enrolled to both SSO and OLS")
  }
}

function SSOGCONotOLSEnrolled(Policy)
{
                Log.Message("Policy " + Policy + "is not enrolled to both SSO and OLS")
                 //enter email id by concatenating policy number retrieved from DB
                 objectKeys(txtSSOEmail, "nivedita.qa+" + Policy + "@gmail.com")
                //click on send code
                objectClick(pnlSSOSendvalcode)
                dynamicWait(btnSSOOk, 6)
                //click on ok button in alert pop up for validation code
                objectClick(btnSSOOk)
                //wait for validation code field
                dynamicWait(txtSSOValidationcode, 3)
                //calling the result from batch email message queue table for the validation code
                var resultbatch = getValidationCodeBatch(Policy)
                //enter validation code
                objectKeys(txtSSOValidationcode, resultbatch)
                //click on verify information
                objectClick(pnlSSOVerifyinfo)
                //wait for billing link in billing page
                dynamicWait(lnkBilling, 3)
                Log.Message("SSO enrollment for policy " + Policy + " is successful")      
}