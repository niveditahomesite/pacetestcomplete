﻿//USEUNIT ImportUnits

/*********************************************************************************************************************************************************************************
'
'     NAME                      :   DBQueries  
'     AUTHOR                    :   Nivedita, Yash 
'     DATE                      :   3/22/2019
'     DESCRIPTION               :   This file consists of database queries later has to be maintained in DB 
'
'*********************************************************************************************************************************************************************************/

testcaseName = Project.TestItems.Current.Name
    //Split testcase name using _
    testcaseSplit = []
    testcaseSplit = testcaseName.split("_")
   partner = testcaseSplit[2]
   form = testcaseSplit[3]
   //movingTo can have values home or renter
   
 function moveFlow(movingTo,homeOrCondo)
 {
   polnumber = intializeExecution("","Y")
   
   //create a function to click on either renter or hoem based on the movingTo value 
   clickMore(movingTo)
   //Moving header and click on more on home or renter based on movingTo
    
   if ((partner != "PGR") && (partner != "ESU"))
   {     
   allPartnerMove(movingTo,homeOrCondo)         
   }
   
   if (partner == 'PGR')
   { 
      pgrMove(movingTo,homeOrCondo)    
   }
   
   if (partner == 'ESU')
   {     
   esuMove(movingTo,homeOrCondo)
      }
   
   createFolder("Step 3: Logout and close the application",closeExecution,"")
   
 }
 
 function allPartnerMove(movingTo,homeOrCondo)
 {
    switch (form) {
             
            case "2":
            //Negative scenario
                Log.Message("Moving button should not be available")
                
                break;
                
            case "3":
            //Click on moving and more button for condo and home and later click on moving again and clcik on more for renter 
                Log.Message("Call us flow")
                callFlow()
                break;
                
            case "4":
            //Click on moving and more button for condo and home and later click on moving again and clcik on more for renter and proceed to renter tarnsfer
            if (movingTo = "renter")
            {
              Log.Message("renter transfer")
            }
            else 
            {
              Log.Message("Call us flow")
            }

                
                break;
                
            case "6":
                Log.Message("Call us flow")
                break;
                
            case "9":
                Log.Message("Moving button should not be available")
                break;
                
            default:
                Log.Error("Invalid user form")
        }
 }
 
 function pgrMove(movingTo,homeOrCondo)
 {
 switch (form) {
             
            case "2":
            //Negative scenario
                Log.Message("Moving button should not be available")
                break;
                
            case "3":
            //Click on moving and more button for condo and home and later click on moving again and clcik on more for renter
               URLFlow(homeOrCondo) 
                Log.Message("URL Flow")
                break;
                
            case "4":
            //Click on moving and more button for condo and home and later click on moving again and clcik on more for renter and proceed to renter tarnsfer
                if (movingTo = "renter")
            {
              Log.Message("renter transfer")
            }
            else 
            {
              URLFlow(homeOrCondo)
            
              Log.Message("url flow")
            }
                
            case "6":
            URLFlow(homeOrCondo) 
                Log.Message("URL Flow")
                break;
            case "9":
                Log.Message("Moving button should not be available")
                break;
            default:
                Log.Error("Invalid user form")
        }
 }
 
 function esuMove(movingTo,homeOrCondo)
 {
   
 switch (form) {
             
            case "2":
            //Negative scenario
                Log.Message("Moving button should not be available")
                break;
                
            case "3":
            //Click on moving and more button for condo and home and later click on moving again and clcik on more for renter 
                Log.Message("Call us flow")
                break;
                
            case "4":
            //Click on moving and more button for condo and home and later click on moving again and clcik on more for renter and proceed to renter tarnsfer
                  if (movingTo = "renter")
            {
              Log.Message("renter transfer")
            }
            else 
            {
              URLFlow(homeOrCondo) 
            
              Log.Message("url flow")
            }
                
            case "6":
                Log.Message("Call us flow")
                break;
            case "9":
                Log.Message("Moving button should not be available")
                break;
            default:
                Log.Error("Invalid user form")
        }
 }