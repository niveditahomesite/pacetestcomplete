﻿//USEUNIT Import_Units

function CAT_Demo_Testcase()
{
  Log.AppendFolder("Step 1: Launch the browser and Navigate to the application")
  Navigate_Application(env,browser)
  if(!Login_CAT(env))
  {
    return false;  
  } 
  Log.PopLogFolder()
  
    
  Log.AppendFolder("Step 2: Logout from the application and close the browser")
  Logout_CAT()
  Browser_Close()  
  Log.PopLogFolder()
}
