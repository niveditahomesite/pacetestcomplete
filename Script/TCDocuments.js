﻿//USEUNIT ImportUnits

/*********************************************************************************************************************************************************************************
'
'     NAME                      :   Documents  
'     AUTHOR                    :   Nivedita, Yash 
'     DATE                      :   5/2/2019
'     DESCRIPTION               :   This file consists of test cases for Documents and EOI for all users 
'
'*************************************************************************************************   *******************************************************************************/

//Send EOI to policy holder, email address and fax number - Branding test cases for validating partner

function TCSendEOI()
{
  try 
{
     
    //Step 1: Login to the application  
    polnumber = intializeExecution("","Y")
        
    createFolder("Step 2: Click on Documents and Proof of Insurance in the header", clickEOI,"")
    
    createFolder("Step 3: Validate the form type in the header", validateHeaderEOI,polnumber)
 
    createFolder("Step 4: Validate the policy holder email address displays in the -Send this to policy holder email address",validateEmailEOI,polnumber)
    
    createFolder("Step 5: Validate the summary shows the email address, recipient and fax number entered and does not display the policy holder email address", validateSummaryEOI, polnumber)
        
    createFolder("Step 6: Validate the notification for email address, recipient, fax number entered in step 5 and the form type",validateNotificationEOI,polnumber)
        
    createFolder("Step 7: Validate the partner branding email address", validatePartnerEmailAdressEOI,polnumber)
        
    createFolder("Step 8: Logout and close the application",closeExecution,"")
     
       
    }catch(e){

    Log.Error("Test case execution failed")  
    //browserClose()

}
}