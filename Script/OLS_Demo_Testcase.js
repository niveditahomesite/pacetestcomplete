﻿//USEUNIT Import_Units
/*===================================================================================================================
Script Name:               Testcase for Login into OLS application
Created By:                Cigniti
Steps To Automate: 
1: Launch the browser and Navigate to the application
2: Enter the policy number
3: Logout from the application and close the browser
*/


function OLS_Demo_Testcase()
{
  Log.AppendFolder("Step 1: Query to check the enrollment status")
  //var dbPolicyEnroll = {qa:QA_dbPolicyEnroll, uat:UAT_dbPolicyEnroll}
  Status = db_query()
  Log.PopLogFolder() 

  
  Log.AppendFolder("Step 2: Launch the browser and Navigate to the application")
  Navigate_Application(env,browser)
  Log.PopLogFolder()
  
  Log.AppendFolder("Step 3:If not enrolled,enroll the policy else update password and login")  
    if(Status[2] == null)
    {
    Policy_Enrollment_OLS()
    }
    else if(Status[2]!=null)
    {
    db_UpdatePassword = "update customer_ENC set password = 'f7ujETryW/cs3TKu0aHSqg==' where CustomerId in (SELECT CustomerId FROM Policy WHERE PolicyNumber = "+Status[0]+")"
    Update_Password(dbPolicyEnroll,"PolicyServices",db_UpdatePassword)
    Login_OLS(Status[3])
    }
    else
    {
    Log.Error("No password value")
    }
  Log.PopLogFolder()
  
//  Log.AppendFolder("Step 3: NameChange FAQ")
//  Help_Center_NameChange()
//  Log.PopLogFolder()  
   
  Log.AppendFolder("Step 4: Logout from the application and close the browser")
  Logout_Application()  
  Log.PopLogFolder()
    
}

