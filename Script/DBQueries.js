﻿//USEUNIT ImportUnits

/*********************************************************************************************************************************************************************************
'
'     NAME                      :   DBQueries  
'     AUTHOR                    :   Nivedita, Yash 
'     DATE                      :   3/22/2019
'     DESCRIPTION               :   This file consists of database queries later has to be maintained in DB 
'
'*********************************************************************************************************************************************************************************/

//DB Queries 

//Pace Service log retrieve a policy dbPolicyService
dbGetPolicyNumber = "Select top 1 * FROM HS_SERVICE_INFO.dbo.PACE_Service_Log(NOLOCK) where PolicyNumber is not NULL ORDER BY TransactionDate DESC"

//Get Policy brand +policyNumber 
dbGetPolicyBrand = "Select top 1 * from ISO_PROD..vwpolicyBrand(NOLOCK) where Policy_Number ="

//Get Policy enrollment status 
dbGetEnrollmentStatus = "Select top 1 * from dbo.customer_ENC(NOLOCK) join Policy(NOLOCK) on customer_ENC.CustomerId = dbo.Policy.CustomerId where dbo.Policy.PolicyNumber = "

// Get Policy Number from HS_Service_Info Table 
dbGetPolicyNumberHS = "select top 1 * from ISO_Prod..HS_CLIENT_SEARCH h(NOLOCK), ISO_PROD..vwpolicyBrand v (NOLOCK) where h.POLICY_NUMBER = v.Policy_Number and h.POLICY_NUMBER<>''order by h.POLICY_NUMBER"

//Get policy information - brand, form, amfm account, state 
dbGetPolicyInfo = "select top 1 * from ISO_Prod..HS_CLIENT_SEARCH h(NOLOCK), ISO_PROD..vwpolicyBrand v (NOLOCK) where h.POLICY_NUMBER = v.Policy_Number and h.POLICY_NUMBER ="

//Get policy number based on partner brand 
dbGetPolicyNumberBrand = "select top 1 * from ISO_Prod..HS_CLIENT_SEARCH h(NOLOCK), ISO_PROD..vwpolicyBrand v (NOLOCK) where h.POLICY_NUMBER = v.Policy_Number and v.BRAND ='"

//Append to get policy number query from HS Client search table for String
dborderbyString = "'order by h.POLICY_NUMBER DESC"

//Append to get policy number query from HS Client search table for Int 
dborderbyInt = " order by h.POLICY_NUMBER DESC"

//Append to get policy number query from HS Client search table for in 
dborderbyIn = ") order by h.POLICY_NUMBER DESC"

//Get policy number based on form
dbGetPolicyNumberForm = "select top 1 * from ISO_Prod..HS_CLIENT_SEARCH h(NOLOCK), ISO_PROD..vwpolicyBrand v (NOLOCK) where h.POLICY_NUMBER = v.Policy_Number and h.FormNumber ="

//Get policy number based on amfm account number 
dbGetPolicyNumberAmf = "select top 1 * from ISO_Prod..HS_CLIENT_SEARCH h(NOLOCK), ISO_PROD..vwpolicyBrand v (NOLOCK) where h.POLICY_NUMBER = v.Policy_Number and h.amf_account_number in ("

//Append to query when passing a  string value
dbGetPolicyNumberFormString = "' and h.FormNumber = "

//Append to query when passing a in string value
dbGetPolicyNumberFormIn = ") and h.FormNumber = "

//Update password to Policy@123
dbUpdatePassword = "update customer_ENC set password = 'f7ujETryW/cs3TKu0aHSqg==' where CustomerId in (SELECT CustomerId FROM Policy (NOLOCK)WHERE PolicyNumber = "

//Get if the policy is enrolled 
dbGetPolicyEnrolled = "SELECT top 1 * FROM [Hrz_DB_Server].ISO_Prod.dbo.HS_CLIENT_SEARCH bp WITH (NOLOCK) left JOIN dbo.Policy p ON bp.POLICY_NUMBER = p.PolicyNumber WHERE bp.POLICY_NUMBER = "

//Get validation code from batch email message queue table 
dbGetValidationCode = "select top 1 * from ISO_PROD.dbo.BatchEmailMessageQueue (nolock) where PolicyNo = '"

//Append to batch email message queue table 
dborderbymessage = "'ORDER BY MessageID DESC"

//Get partner brnading information from BatchEmailPartnerInfo table 
dbBatchEmailPartnerInfo = "select top 1 * from ISO_prod.dbo.BatchEmailPartnerInfo i(NOLOCK), ISO_PROD..BatchEmailMessageQueue q (NOLOCK) where i.PartnerId = q.PartnerID and PolicyNo = '"

