﻿//USEUNIT ImportUnits
/*********************************************************************************************************************************************************************************
'
'     NAME                      :   GlobalVariables  
'     AUTHOR                    :   Nivedita, Yash 
'     DATE                      :   3/22/2019
'     DESCRIPTION               :   This file consists of global variables  
'
'*********************************************************************************************************************************************************************************/

Project_Path = Project.Path
Output_Folder = Project_Path + "\\OUTPUT\\"

/*--------------------------------QA--------------------------------------------------------------*/
//QA URL's OLS
QA_TMS = "http://qa-tmsinsurance.amfamconnect.homesite.com/OnlineServicing/Welcome.aspx#Login"
QA_NGEN = "http://qa-ngen.camelot.local/OnlineServicing/Welcome.aspx#Login"
QA_GEN = "http://qa-general/OnlineServicing/Welcome.aspx#Login"
QA_EFC = "http://qa-effective.amfamconnect.com/OnlineServicing/Welcome.aspx#Login"
QA_BNG = "http://qa-bungalow/OnlineServicing/Welcome.aspx#Login"
QA_INS = "http://qa-insuritas.amfamconnect.com/OnlineServicing/Welcome.aspx"
QA_COV = "https://qa-covermystuff.amfamconnect.com/OnlineServicing/Welcome.aspx#Login"
QA_NW = "http://qa-nationwide/OnlineServicing/Welcome.aspx#Login"
QA_THS = "http://qa-ths.camelot.local/OnlineServicing/Welcome.aspx#Login"
QA_GCO = "http://qa-ici.camelot.local/OnlineServicing/Welcome.aspx#Login"
QA_PGR = "http://qa-progressivedirect/OnlineServicing/Welcome.aspx#Login"
QA_MHL = "http://qa-mhl.camelot.local/OnlineServicing/Welcome.aspx#Login"
QA_HRM = "http://qa-wwwhs.camelot.local/OnlineServicing/Welcome.aspx#Login"
QA_AMZ = "https://qa-affinity2.amfamconnect.com/OnlineServicing/Welcome.aspx#Login"
QA_LBM = "http://qa-lbm.homesite.com/OnlineServicing/Welcome.aspx#Login"
QA_HS = "http://qa-wwwhs.camelot.local/OnlineServicing/Welcome.aspx#Login"
QA_INF = "http://qa-infinity.camelot.local/OnlineServicing/Welcome.aspx#Login"
QA_AMP = "http://qa-amp.homesite.com/OnlineServicing/Welcome.aspx#Login"
QA_COM = "http://qa-compare.homesite.com/OnlineServicing/Welcome.aspx#Login"
QA_MMT = "http://qa-movement.homesite.com/OnlineServicing/Welcome.aspx#Login"
QA_MEDA = "http://qa-wwwhs.camelot.local/OnlineServicing/Welcome.aspx#Login"
QA_ISA = "http://qa-wwwhs.camelot.local/OnlineServicing/Welcome.aspx#Login"
QA_LDT = "http://qa-lendingtree.amfamconnect.com/OnlineServicing/Welcome.aspx"
QA_GMAC = "http://qa-gmac123/OnlineServicing/welcome.aspx#Login"
QA_HMGE = "http://qa-hmge.amfamconnect.com/OnlineServicing/Welcome.aspx#Login"
QA_MAT = "http://qa-matic.amfamconnect.com/OnlineServicing/welcome.aspx#Login"
QA_ELT = "http://qa-electric.amfamconnect.com/OnlineServicing/Welcome.aspx#Login"
QA_21st = "http://qa-wwwhs.camelot.local/OnlineServicing/Welcome.aspx#Login"
QA_APR = "http://qa-apparent.homesite.com/OnlineServicing/Welcome.aspx#Login"
QA_AMFM = "http://qa-mypolicy.amfamconnect.com/OnlineServicing/Welcome.aspx#Login"
QA_ELE = "http://qa-elephant/OnlineServicing/Welcome.aspx#Login"

//QA SSO Partners 
QA_SSOGCO = "http://qa-ici.camelot.local/SAML.Idp.TestHarness/Geico/Default.aspx"
QA_SSOPGR = "http://qa-progressivedirect/SAML.Idp.TestHarness/Progressive/Default.aspx"
QA_SSOAMFM = "http://qa-mypolicy.amfamconnect.com/SAML.Idp.TestHarness/AmFamConnect/Default.aspx"

//QA Superuser and CAT 
QA_SU = "http://qa-onlineservicinginternal/OnlineServicing/SuperUserLogin.aspx"
QA_CAT = "http://qa-onlineservicinginternal/OnlineServicing/pacecatuserlogin.aspx"

/*-------------------------------UAT--------------------------------------------------------------------*/
//UAT URL's OLS
UAT_TMS = "https://uat-tmsinsurance.amfamconnect.homesite.com/OnlineServicing"
UAT_NGEN = "https://uat-ngen.homesite.com/OnlineServicing"
UAT_GEN = "http://uat-general/OnlineServicing/Welcome.aspx"
UAT_EFC = "https://uat-effective.amfamconnect.com/OnlineServicing/Welcome.aspx#Login"
UAT_BNG = "https://uat-bungalow.homesite.com/OnlineServicing/Welcome.aspx#Login"
UAT_INS = "https://uat-insuritas.amfamconnect.com/OnlineServicing"
UAT_COV = "https://uat-covermystuff.amfamconnect.com/OnlineServicing/Welcome.aspx#Login"
UAT_NW = "http://uat-nationwide.camelot.local/OnlineServicing/Welcome.aspx#Login"
UAT_THS = "https://uat-ths.homesite.com/OnlineServicing/Welcome.aspx#Login"
UAT_GCO = "http://uat-ici.camelot.local/OnlineServicing/Welcome.aspx#Login"
UAT_PGR = "http://uat-progressivedirect/OnlineServicing/Welcome.aspx#Login"
UAT_MHL = "http://uat-mhl.homesite.com/OnlineServicing/Welcome.aspx"
UAT_HRM = "http://uat-wwwhs.camelot.local/OnlineServicing/Welcome.aspx"
UAT_AMZ = "https://uat-affinity2.amfamconnect.com/OnlineServicing"
UAT_LBM = "http://uat-mhl.homesite.com/OnlineServicing/Welcome.aspx"
UAT_HS = "https://uat-wwwhs.camelot.local/OnlineServicing/Welcome.aspx#Login"
UAT_INF = "https://uat-infinity.homesite.com/OnlineServicing"
UAT_AMP = "https://uat-amp.amfamconnect.homesite.com/OnlineServicing"
UAT_COM = "https://uat-compare.homesite.com/OnlineServicing/Welcome.aspx#Login"
UAT_MMT = "https://uat-movement.homesite.com/OnlineServicing/Welcome.aspx#Login"
UAT_LDT = "https://uat-lendingtree.amfamconnect.com/OnlineServicing/Welcome.aspx#Login"
UAT_GMAC = "https://gmac123.homesite.com/OnlineServicing/Welcome.aspx#Login"
UAT_HMGE = "http://uat-hmge.amfamconnect.com/OnlineServicing/Welcome.aspx#Login"
UAT_MAT = "https://uat-matic.amfamconnect.com/OnlineServicing"
UAT_ELT = "https://uat-electric.amfamconnect.com/OnlineServicing/Welcome.aspx#Login"
UAT_21st = "https://uat-wwwhs.camelot.local/OnlineServicing/Welcome.aspx#Login"
UAT_APR = "http://uat-apparent.homesite.com/OnlineServicing/Welcome.aspx#Login"
UAT_AMFM = "https://uat-servicing.amfamconnect.com/OnlineServicing/Welcome.aspx#Login"
UAT_ELE = "https://uat-elephant.camelot.local/OnlineServicing/Welcome.aspx#Login"
UAT_MEDA = ""
UAT_ISA = ""

//UAT SSO Partners 
UAT_SSOGCO = "http://uat-ici.camelot.local/SAML.Idp.TestHarness/Geico/Default.aspx"
UAT_SSOPGR = "https://test.progressivedirect.homesite.com:4434/SAML.Idp.TestHarness/Progressive/Default.aspx"
UAT_SSOAMFM = "https://uat-mypolicy.amfamconnect.com/SAML.Idp.TestHarness/AmFamConnect/Default.aspx"

//UAT Superuser and CAT 
UAT_SU = "https://uat-onlineservicinginternal.homesite.com/OnlineServicing/SuperUserLogin.aspx"
UAT_CAT = "https://uat-onlineservicinginternal.homesite.com/OnlineServicing/pacecatuserlogin.aspx"


/*---------------------DB Server names------------------------------------*/
QA_dbISO = "iso_db_qa\\SQL01"
UAT_dbISO = "iso_db_uat\\SQL01"
QA_dbSvcinfo = "svcinfo_db_qa\\sql06"
UAT_dbSvcinfo = "svcinfo_db_uat\\sql06"
QA_dbWeb = "web_db_qa\\SQL04"
UAT_dbWeb = "web_db_uat\\SQL04"
QA_dbDMBilling = "dmb_db_qa\\SQL02"
UAT_dbDMBilling = "dmb_db_uat\SQL02"

/*-------------------Environment and browser-------------------------------*/
env = "uat"
//browser = "iexplore"

/*---------------------XML------------------------------------*/
QA_psnap = "http://camqapds01/HomeSite.Infrastructure/Policy/.XML/"
UAT_psnap = "http://camuatpds01/HomeSite.Infrastructure/Policy/.XML/"
QA_bsnap = "http://camqapds01/HomeSite.Infrastructure/Receivables/.XML/"
UAT_bsnap = "http://camuatpds02/HomeSite.Infrastructure/Receivables/.XML/"

/*--------------------Credentials--------------------------------------------------*/
QA_SU_username = "cig_nived.nimmagadda"
QA_SU_pwd = "Apr@2019"
UAT_SU_username = "cig_nived.nimmagadda"
UAT_SU_pwd = "Apr@2019"
QA_CAT_username = "CATQA_hung.lee"
QA_CAT_pwd = "CaUjHE6bwv"
UAT_CAT_username = "CATUAT_hung.lee"
UAT_CAT_pwd = "vCNfHPx2dF"
Password_default = "Policy@123"