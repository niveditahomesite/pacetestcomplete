﻿//USEUNIT Import_Units
/*===================================================================================================================
Script Name:               Testcase for Login into Super user
Created By:                Cigniti
Steps To Automate: 
1: Launch the browser and Navigate to the application
2: Enter the policy number
3: Logout from the application and close the browser
*/


function SU_Demo_Testcase()
{
  Log.AppendFolder("Step 1: Launch the browser and Navigate to the application")
  Navigate_Application(env,browser)
  if(!Login_SU(env))
  {
    return false;  
  } 
  Log.PopLogFolder()
  
  Log.AppendFolder("Step 2: Enter the policy number and view the policy")
  Policy = db_query()
  Dynamic_Wait(txtSUPN,2)
  Object_Keys(txtSUPN,Policy[0])
  Object_Keys(txtSUPN,"[Enter]")
  Dynamic_Wait(txtnodeSUCustomerstatus,5)
  Log.Checkpoint(txtnodeSUCustomerstatus.contentText)
  Object_Click(btnSUViewPolicy)
  Log.PopLogFolder()
    
  Log.AppendFolder("Step 3: Logout from the application and close the browser")
  Logout_SU()
  Browser_Close()  
  Log.PopLogFolder()
}

