﻿//USEUNIT Import_Units

function SSOGCO_Demo_Testcase()
{
  var dbDMBilling = {qa:QA_dbDMBilling, uat:UAT_dbDMBilling}
  db_getPolicyNumber_dbDMBilling_SSOGCO = "Select top 1  RC_POLICY_NO from d040100  WITH (NOLOCK)join D044000 WITH (NOLOCK) on RC_Policy_No = RPC_Policy_No where RC_Billing_Cycle <> '10' and RC_ACCOUNT_NO IN (14000,14400,14310)and RC_POLICY_EFF_DATE < 20190301 order by RC_POLICY_EFF_DATE DESC"
  var Policy_No = getPolicyNumber(dbDMBilling,"DM_Billing_Prod",db_getPolicyNumber_dbDMBilling_SSOGCO,"RC_POLICY_NO")
  Policy_No = Policy_No.toString();
  Policy_No = Policy_No.split(" ");
  var Result = Policy_No[0]
  Log.Message(Result)
  
  Log.AppendFolder("Step 1: Launch the browser and Navigate to the application")
  Navigate_Application(env,browser)
  if(!Login_SSOGCO(Result))
  {
    return false;  
  } 
  Log.PopLogFolder()
  
  Log.AppendFolder ("Step 2: Enrollment")
  if(!lnkBilling.Exists)
  {
    xml_values = TestXMLDOM(Result,"PolicyNumber","LastNameOfCustomer","DateOfBirth01","ZipCode1")
    aqUtils.Delay(1000)
    Object_Keys(txtSSOLN,xml_values[1])
    Object_Keys(txtSSODOB,aqConvert.DateTimeToFormatStr(xml_values[2].substring(0,10), "%m/%d/%Y"))
    Object_Keys(txtSSOZipcode,xml_values[3])
    Object_Click(pnlSSOVerifyinfo)
    Dynamic_Wait(lnkBilling,3)
    //AddEFT()
    Object_Click(lnkBilling)
  }
  else
  {
   //AddEFT()
   Object_Click(lnkBilling)
  }
 
  Log.AppendFolder("Step 3: Logout from the application and close the browser")
  Logout_SSO()
  Browser_Close()  
  Log.PopLogFolder()
}


