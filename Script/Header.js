﻿//USEUNIT ImportUnits
/*********************************************************************************************************************************************************************************
'
'     NAME                      :   Header
'     AUTHOR                    :   Nivedita, Yash 
'     DATE                      :   4/22/2019
'     DESCRIPTION               :   This file is a functional and object repository to Header for all partners and users 
'
'*********************************************************************************************************************************************************************************


/*********************************Header page objects********************************/

var lnkBilling = Aliases.Browser.Header_All.header.lnkBilling
var lnkClaims = Aliases.Browser.Header_All.header.lnkClaims
var lnkDocuments = Aliases.Browser.Header_All.header.lnkDocuments
var lnkHelpcenter = Aliases.Browser.Header_All.header.lnkHelpcenter
var lnkLogo = Aliases.Browser.Header_All.header.lnkLogo
var lnkMyProfile = Aliases.Browser.Header_All.header.lnkMyProfile
var lnkPOI = Aliases.Browser.Header_All.header.lnkPOI
//var panelPolicyInfo = Aliases.Browser.Header_All.header.panelPolicyInfo
var txtSearchBox = Aliases.Browser.Header_All.header.txtSearchBox
var btnSearchIcon = Aliases.Browser.Header_All.header.btnSearchIcon
var btnMovingTransferPolicy = Aliases.Browser.Header_All.header.btnMovingTransferPolicy
var lnkGoBackSU = Aliases.Browser.Header_All.header.lnkGoBackSU /*check if for SSO and OLS same object can be used */
var headerPOI =  Aliases.Browser.pgEOI.headerPOI

/*********************************Header page functions********************************/

//To click EOI 
function clickEOI()
{
  //Click on Documents link
  objectHoverMouse(lnkDocuments)
  //Click on POI link 
  objectClick(lnkPOI)
  //Validate if navigated to EOI page 
  dynamicWait(headerPOI, 5)
  if (headerPOI.exists)
  {
    Log.Message("Navigated to EOI Page")
  }
  else 
  {
    Log.Error("Navigation to EOI Page failed")
  }
}






