﻿//USEUNIT ImportUnits

/*********************************************************************************************************************************************************************************
'
'     NAME                      :   UtilityLibrary  
'     AUTHOR                    :   Nivedita, Yash 
'     DATE                      :   3/22/2019
'     DESCRIPTION               :   This file consists of common functions used across application
'
'*********************************************************************************************************************************************************************************/

//Launch IE browser close any open browser 
function browserLaunch() {
    //Check if the browser is open 
    var BrowserProcess = Sys.FindChild("ProcessName", "iexplore", 2)
    //if browser is not open 
    if (BrowserProcess.Exists == false) {
        //Launch IE browser  
        Browsers.Item(btIExplorer).Run();
        Log.Message("Browser launched successfully")
    } else {
        Log.Message("Browser already exists")
        //Close browser
        browserClose()
        //Launch IE browser 
        Browsers.Item(btIExplorer).Run();
    }
}

//Navigate to homesite url 
function launchURL() {
    //Navigate to Homesite URl
    Browsers.Item(btIExplorer).Navigate(Homesite_Url);
    //Dynamic wait for the username text box 
    dynamicWait(Textbox_Userid, 3)
    //If username text box exists return true else return false 
    if (Textbox_Userid.Exists == true) {
        Log.Message("Application launched");
        return true;
    } else {
        Log.Error("Couldn't launch the application");
        return false;
    }
}

//Reset browser to default value 
function resetAppDefault() {
    Log.AppendFolder("Navigating to the Home page")
    Browsers.Item(btChrome).Navigate(Project.Variables.Home_URL);
    Log.PopLogFolder()
}

//Close the browser
function browserClose() {
    Sys.Browser("*").Close()
}

//Generate a random number
function randInt() {
    return Math.round(Math.random() * 1000000);
}

//Wait for object 
//Parmeter - object and timeout in milliseconds (If no time is specified,then function waits for default time i.e 60seconds )
function dynamicWait(object, timeout) {
    timeout = timeout || 10;
    for (var i = 1; i <= timeout; i++) {
        //wait for object   
        if (object.Exists == false) {
            aqUtils.Delay(1000);
        } else {
            break;
        }
    }
}

//To verify whether the object exists, if exists click on the object.
function objectClick(objectName) {
    var objectValue = objectName.Name;
    //If able to identify object 
    if (objectName.Exists == true) {
        //click object and log message 
        objectName.Click();
        Log.Event("Object exists clicked on the object: " + objectValue)
        return true;
    } else {
        //object does not exist 
        Log.Error("Object doesn't exist couldn't click on the object" + objectValue)
        return false;
    }
}

//To verify whether the object exists, if exists click on the object and enter the input value.
function objectKeys(objectName, inputValue) {
    var objectValue = objectName.Name;
    //if input value is not defined 
    if (inputValue != null) {
        //Click on object if able to identify 
        if (objectName.Exists == true) {
            //Log.LockEvents()
            //Click on object 
            objectName.Click();
            //Enter value in the text box
            objectName.Keys("^a");
            objectName.Keys(inputValue);
            Log.Event("Object exists " + objectValue + ". Clicked on the object and entered the value " + inputValue)
        } else {
            //Log.UnlockEvents()
            Log.Error("Object and Input Value doesn't exist. Couldn't click on the object and enter the input value" + inputValue)
        }
    } else {
        Log.Error("The input value is null ")
    }
}

function createFolder(folderTitle, functionName,par1)
{
   Log.AppendFolder(folderTitle)
   if (!par1)
   {
    functionName()
    }
    else 
    {
      functionName(par1)
    }
    Log.PopLogFolder()

}
//To compare instring  
function inString(object,compString)
{
  
   var objContentText = object.contentText; 
   if (!objContentText)
   {
     Log.Error("Unable to find the object "+object)
   }
   else 
   {
      var pos = objContentText.search(compString) 
      
      
      //Log.Message(pos)
      if (pos >= 0)
      {
        Log.Message("Text - "+compString+ " is displayed in the string - "+objContentText)
        return true
      }
      else 
      {
        Log.Error("Text - "+compString+" is not displayed in the string - "+objContentText)
        return false
      }
   }
     
}
//To compare instring - TODONN - Add comments and format 
function inStringSpecialChar(object,compString)
{
  
   var objContentText = object.contentText
   //var objContentText = objContentText.replace("-", "")
   //var objContentText = objContentText.replace("(", "")
   //var objContentText = objContentText.replace(")", "")
   //var objContentText = objContentText.replace(" ", "")
   
    
   if (!objContentText)
   {
     Log.Error("Unable to find the object "+object)
   }
   else 
   {
        
      var pos = aqString.Contains(objContentText, compString, 0, true)
      //compString.localeCompare(objContentText)
      //Log.Message(pos)
      if (pos >= 0)
      {
        Log.Message("Text - "+compString+ " is displayed in the string - "+objContentText)
        return true
      }
      else 
      {
        Log.Error("Text - "+compString+" is not displayed in the string - "+objContentText)
        return false
      }
   }
     
}
//To verify whether the object exists, if exists select the item from the select dropdown
function objectSelectItem(objectName, selectValue) {
    var objectValue = objectName.Name;
    //Dropdown value to be selected 
    if (selectValue != null) {
        //Validate if the object exists   
        if (objectName.Exists == true) {
            //Click on the object 
            objectName.Click();
            aqUtils.Delay(1000)
            //Select the value from the dropdown 
            objectName.ClickItem(selectValue);
            Log.Event("Object exists. Selected the item " + objectValue)
        } else {
            //Unable to identify object 
            Log.Error("Object doesn't exist couldn't click on the object " + objectValue)
        }
    } else {
        Log.Error("The input value is not defined for the dropdown selection")
    }
}

//To verify whether the object exists, if exists hover the mouse on the object
function objectHoverMouse(objectName) {
    var objectValue = objectName.Name;
    //if object exists 
    if (objectName.Exists == true) {
        //Hoverover the object 
        objectName.HoverMouse();
        Log.Event("Object exists. Hovered the mouse over the object " + objectValue)
        return true 
    } else {
        //Unable to identify object 
        Log.Error("Object doesn't exist. Couldn't hover the mouse over the object " + objectValue)
        return false 
    }
}

//To create a folder to save the logs
function folderCreation() {
    var mFolder = Project_Path + "\\OUTPUT\\"
    var sFolderName = aqDateTime.Today();
    var sFolder = aqString.Concat(mFolder, sFolderName)
    var soutFolder = aqDateTime.Time();
    var sFolderName1 = aqString.Concat(sFolderName, "_")
    var text = aqString.Concat(sFolderName1, soutFolder)
    text = aqString.Concat("\\", text)
    //Log.Message(text)
    text = aqString.Replace(text, ":", "_")
    currentTimeFolder = aqString.Concat(sFolder, text)
    if (!mFolder.Exists); {
        aqFileSystem.CreateFolder(mFolder)
    }
    if (!sFolder.Exists) {
        aqFileSystem.CreateFolder(sFolder);
    }
    aqFileSystem.CreateFolder(currentTimeFolder);
    //Log.Message(currentTimeFolder)
    return currentTimeFolder
}

//To change the date format for folder creation 

function folderCreationDateFormat() {
    var mFolder = Output_Folder
    var sFolderName = aqDateTime.Today();
    var sFolderName_Replace = aqString.Replace(sFolderName, "/", "-")
    var sFolder = aqString.Concat(mFolder, sFolderName_Replace)
    var soutFolder = aqDateTime.Time();
    var sFolderName1 = aqString.Concat(sFolderName_Replace, "_")
    var text = aqString.Concat(sFolderName1, soutFolder)
    text = aqString.Concat("\\", text)
    text = aqString.Replace(text, ":", "_")
    currentTimeFolder = aqString.Concat(sFolder, text)
    if (!mFolder.Exists); {
        aqFileSystem.CreateFolder(mFolder)
    }
    if (!sFolder.Exists) {
        aqFileSystem.CreateFolder(sFolder);
    }
    aqFileSystem.CreateFolder(currentTimeFolder);
    return currentTimeFolder
}

//To save log 
function saveLog(Testcase) {
    SavePath = folderCreation();
    Log.SaveResultsAs(SavePath + "\\" + Testcase + "_Log.mht", lsMHT)
}

//To save consolidated log 
function saveConsolidatedLog(Testcase) {
    SavePath = folderCreationDateFormat();
    Log.SaveResultsAs(SavePath + "\\" + "Log_Report.mht", lsMHT)
}

//Get Test case name and determine the user type  
function getTestcasename() {
    try {
        //Get testcase name   
        testcaseName = Project.TestItems.Current.Name
        //Split testcase name using _
        testcaseName = testcaseName.split("_")
        //if test case name has only user 
        return testcaseName
    } catch (e) {
        Log.Error("Unable to fetch the test case name")
    }
}

//To be called at end of the test case execution for loging off and closing browser 
function closeExecution() {
    try {
        //Get testcase name   
        testcaseNameClose = Project.TestItems.Current.Name
        //Split testcase name using _
        testcaseSplitClose = []
        testcaseSplitClose = testcaseName.split("_")
        //Get usertype from test case name 
        userType = testcaseSplitClose[0]
        //switch case to logout from different user types 
        switch (userType) {
            //logout from OLS 
            case "OLS":
                logout()
                Log.Message("Logged out from OLS User")
                break;
                //logout from Cat user 
            case "CAT":
                logoutCAT()
                Log.Message("Logged out from CAT User")
                break;
                //logout from superuser
            case "SU":
                logoutSU()
                Log.Message("Logged out from SuperUser")
                break;
                //logout from SSOGCO, SSOPGR, SSOAMFAMS
            default:
                logout()
                Log.Message("Logged out from OLS User")
        }
        browserClose()
        Log.Message("Closing Browser")
    } catch (e) {
        Log.Error("Signout and closing browser failed")
    }
}
//Navigate to URL based on the env and Browser 
function intializeExecution(polnumb, appLogin) {
    var SU = {
        qa: QA_SU,
        uat: UAT_SU
    }
    var CAT = {
        qa: QA_CAT,
        uat: UAT_CAT
    }
    var SSOGCO = {
        qa: QA_SSOGCO,
        uat: UAT_SSOGCO
    }
    var SSOPGR = {
        qa: QA_SSOPGR,
        uat: UAT_SSOPGR
    }
    var SSOAMFM = {
        qa: QA_SSOAMFM,
        uat: UAT_SSOAMFM
    }
    
    Log.AppendFolder("Step 1: Login to the application")
    //Get testcase name   
    testcaseName = Project.TestItems.Current.Name
    //Split testcase name using _
    testcaseSplit = []
    testcaseSplit = testcaseName.split("_")
    //Get the length of test case array - number of parameters passed in test case name 
    tcnamelen = testcaseSplit.length

    switch (tcnamelen) {
        case 3:
            //browser and user are passed in the test case 
            execBr = testcaseSplit[0]
            user = testcaseSplit[1]
            //if policy number is not passed then get a random policy from HS Service info table 
            if (!polnumb) {
                policyNumb = getRandomPolicyHSService()
            } else
            //else consider the policy number passed in the intialise function
            {
                policyNumb = polnumb
            }
            break;
            //browser, user and partner are passed in the test case 
        case 4:
            execBr = testcaseSplit[0]
            user = testcaseSplit[1]
            partner = testcaseSplit[2]
            //if policy number is not passed then get a random policy from HS Service info table based on the form
            if (!polnumb) {
                //if partner is ESU since it's not in brand table and is HS partner -- add esurance amfm number below       
                 switch (partner) {
                      case "ESU":
                          //IE browser
                          policyNumb = getPolicyAmf("1020,1021,1022,1023,1026")
                          break;
                      case "UWAA":
                          //Firefox
                          policyNumb = getPolicyAmf("51000,51001")
                          break;
                          //Chrome browser
                      case "WAL":
                          policyNumb = getPolicyAmf("15001, 15002,15003,15004,15005")
                          break;
                      default:
                          policyNumb = getPolicyPartner(partner)
                          
               
                }
            }
            //else consider the policy number passed in the intialise function
            else {
                policyNumb = polnumb
            }
            break;
            //browser, user, partner and form are passed in the test case
        case 5:
            execBr = testcaseSplit[0]
            user = testcaseSplit[1]
            partner = testcaseSplit[2]
            form = testcaseSplit[3]
            //if policy number is not passed then get a random policy from HS Service info table based on the partner and form
            if (!polnumb) {
                //if partner is ESU since it's not in brand table and is HS partner -- add esurance amfm number below    
                
                switch (partner) {
                    case "ESU":
                        //IE browser
                        policyNumb = getPolicyAmfForm("1020,1021,1022,1023,1026", form)
                        break;
                    case "UWAA":
                        //Firefox
                        policyNumb = getPolicyAmfForm("51000,51001", form)
                        break;
                        //Chrome browser
                    case "WAL":
                        policyNumb = getPolicyAmfForm("15001, 15002,15003,15004,15005", form)
                        break;
                    default:
                        policyNumb = getPolicyBrandForm(partner, form)
                }   
                
            } else
            //else consider the policy number passed in the intialise function
            {
                policyNumb = polnumb
            }
            break;
        default:
            Log.Error("Test case name need to have a _ eg: browser_user_function")
    }
    //Browser name abbreviate mentioned in the test case name 
    switch (execBr) {
        case "IE":
            //IE browser
            execBrowser = "iexplore"
            break;
        case "FF":
            //Firefox
            execBrowser = "firefox"
            break;
            //Chrome browser
        case "CH":
            execBrowser = "chrome"
            break;
        default:
            Log.Error("Browser name defined in the testcase is invalid, browser can be IE, FF or CH ")
    }
    //Array to get policy information from the database to be used for logging in and enrolling 
    status = []
    status = getPolicyInfo(policyNumb)
    //POlicy number from HS Service Info table
    pnumber = status[0]
    //Enroll status from HS Service and policy table 
    enroll = status[8]

    //User passed in the test case
    switch (user) {
        //Superuser navigation based on the env  
        case "SU":
            Browsers.Item(execBrowser).Run(SU[env]);
            Log.Message("Navigating to " + SU[env])
            if (appLogin == "Y") {
                //Login to Superuser       
                loginSU(env, pnumber)
            }
            break;
        case "CAT":
            Browsers.Item(execBrowser).Run(CAT[env]);
            Log.Message("Navigating to " + CAT[env])
            if (appLogin == "Y") {
                //Login to CAT user    
                loginCAT(env, pnumber)
            }
            break;
        case "SSOGCO":
            Browsers.Item(execBrowser).Run(SSOGCO[env]);
            Log.Message("Navigating to " + SSOGCO[env])
            if (appLogin == "Y") {
                //Login to SSO Geico    
                loginSSOGCO(pnumber)
            }
            break;
        case "SSOPGR":
            Browsers.Item(execBrowser).Run(SSOPGR[env]);
            Log.Message("Navigating to " + SSOPGR[env])
            if (appLogin == "Y") {
                //Login to SSO Progressive    
                loginSSOPGR(pnumber)
            }
            break;
        case "SSOAMFM":
            Browsers.Item(execBrowser).Run(SSOAMFM[env]);
            Log.Message("Navigating to " + SSOAMFM[env])
            if (appLogin == "Y") {
                //Login to SSO Amfam    
                loginSSOAMFM(pnumber)
            }
            break;
        case "OLS":
            //login to OLS 
            OLSPartner(execBrowser, status, appLogin)

            break;
        default:
            Log.Error("User is not applicable to navigate to the application")
    }
    Log.PopLogFolder()
    return policyNumb
}


//Navigate to OLS partner specific URL login or enroll based on the policy enrollment status 

function OLSPartner(execBrowser, status, appLogin) {
    //intilalize policy number, brand, enrollment and email from the HS SERVICE INFO
    var pnumb = status[0]
    var policyBrand = status[1]
    var enroll = status[9]
    var email = status[10]

    //Define partners value based in the environment value 

    var TMS = {
        qa: QA_TMS,
        uat: UAT_TMS
    }
    var NGEN = {
        qa: QA_NGEN,
        uat: UAT_NGEN
    }
    var GEN = {
        qa: QA_GEN,
        uat: UAT_GEN
    }
    var EFC = {
        qa: QA_EFC,
        uat: UAT_EFC
    }
    var BNG = {
        qa: QA_BNG,
        uat: UAT_BNG
    }
    var INS = {
        qa: QA_INS,
        uat: UAT_INS
    }
    var COV = {
        qa: QA_COV,
        uat: UAT_COV
    }
    var NW = {
        qa: QA_NW,
        uat: UAT_NW
    }
    var THS = {
        qa: QA_THS,
        uat: UAT_THS
    }
    var GCO = {
        qa: QA_GCO,
        uat: UAT_GCO
    }
    var PGR = {
        qa: QA_PGR,
        uat: UAT_PGR
    }
    var MHL = {
        qa: QA_MHL,
        uat: UAT_MHL
    }
    var HRM = {
        qa: QA_HRM,
        uat: UAT_HRM
    }
    var AMZ = {
        qa: QA_AMZ,
        uat: UAT_AMZ
    }
    var LBM = {
        qa: QA_LBM,
        uat: UAT_LBM
    }
    var HS = {
        qa: QA_HS,
        uat: UAT_HS
    }
    var INF = {
        qa: QA_INF,
        uat: UAT_INF
    }
    var AMP = {
        qa: QA_AMP,
        uat: UAT_AMP
    }
    var COM = {
        qa: QA_COM,
        uat: UAT_COM
    }
    var MMT = {
        qa: QA_MMT,
        uat: UAT_MMT
    }
    var MEDA = {
        qa: QA_MEDA,
        uat: UAT_MEDA
    }
    var ISA = {
        qa: QA_ISA,
        uat: UAT_ISA
    }
    var LDT = {
        qa: QA_LDT,
        uat: UAT_LDT
    }
    var GMAC = {
        qa: QA_GMAC,
        uat: UAT_GMAC
    }
    var HMGE = {
        qa: QA_HMGE,
        uat: UAT_HMGE
    }
    var MAT = {
        qa: QA_MAT,
        uat: UAT_MAT
    }
    var ELT = {
        qa: QA_ELT,
        uat: UAT_ELT
    }
    var st = {
        qa: QA_21st,
        uat: UAT_21st
    }
    var APR = {
        qa: QA_APR,
        uat: UAT_APR
    }
    var AMFM = {
        qa: QA_AMFM,
        uat: UAT_AMFM
    }
    var ELE = {
        qa: QA_ELE,
        uat: UAT_ELE
    }
    

    //Based on the policy brand navigate to the application 
    switch (policyBrand) {
        case "TMS":
            Browsers.Item(execBrowser).Run(TMS[env]);
            Log.Message("Navigating to " + TMS[env])
            break;
        case "NGEN":
            Browsers.Item(execBrowser).Run(NGEN[env]);
            Log.Message("Navigating to " + NGEN[env])
            break;
        case "GEN":
            Browsers.Item(execBrowser).Run(GEN[env]);
            Log.Message("Navigating to " + GEN[env])
            break;
        case "EFC":
            Browsers.Item(execBrowser).Run(EFC[env]);
            Log.Message("Navigating to " + EFC[env])
            break;
        case "BNG":
            Browsers.Item(execBrowser).Run(BNG[env]);
            Log.Message("Navigating to " + BNG[env])
            break;
        case "INS":
            Browsers.Item(execBrowser).Run(INS[env]);
            Log.Message("Navigating to " + INS[env])
            break;
        case "COV":
            Browsers.Item(execBrowser).Run(COV[env]);
            Log.Message("Navigating to " + COV[env])
            break;
        case "NW":
            Browsers.Item(execBrowser).Run(NW[env]);
            Log.Message("Navigating to " + NW[env])
            break;
        case "THS":
            Browsers.Item(execBrowser).Run(THS[env]);
            Log.Message("Navigating to " + THS[env])
            break;
        case "GCO":
            Browsers.Item(execBrowser).Run(GCO[env]);
            Log.Message("Navigating to " + GCO[env])
            break;
        case "PGR":
            Browsers.Item(execBrowser).Run(PGR[env]);
            Log.Message("Navigating to " + PGR[env])
            break;
        case "MHL":
            Browsers.Item(execBrowser).Run(MHL[env]);
            Log.Message("Navigating to " + MHL[env])
            break;
        case "HRM":
            Browsers.Item(execBrowser).Run(HRM[env]);
            Log.Message("Navigating to " + HRM[env])
            break;
        case "AMZ":
            Browsers.Item(execBrowser).Run(AMZ[env]);
            Log.Message("Navigating to " + AMZ[env])
            break;
        case "LBM":
            Browsers.Item(execBrowser).Run(LBM[env]);
            Log.Message("Navigating to " + LBM[env])
            break;
        case "HS":
            Browsers.Item(execBrowser).Run(HS[env]);
            Log.Message("Navigating to " + HS[env])
            break;
        case "INF":
            Browsers.Item(execBrowser).Run(INF[env]);
            Log.Message("Navigating to " + INF[env])
            break;
        case "AMP":
            Browsers.Item(execBrowser).Run(AMP[env]);
            Log.Message("Navigating to " + AMP[env])
            break;
        case "COM":
            Browsers.Item(execBrowser).Run(COM[env]);
            Log.Message("Navigating to " + COM[env])
            break;
        case "MMT":
            Browsers.Item(execBrowser).Run(MMT[env]);
            Log.Message("Navigating to " + MMT[env])
            break;
        case "MEDA":
            Browsers.Item(execBrowser).Run(MEDA[env]);
            Log.Message("Navigating to " + MEDA[env])
            break;
        case "ISA":
            Browsers.Item(execBrowser).Run(ISA[env]);
            Log.Message("Navigating to " + ISA[env])
            break;
        case "LDT":
            Browsers.Item(execBrowser).Run(LDT[env]);
            Log.Message("Navigating to " + LDT[env])
            break;
        case "GMAC":
            Browsers.Item(execBrowser).Run(GMAC[env]);
            Log.Message("Navigating to " + GMAC[env])
            break;
        case "HMGE":
            Browsers.Item(execBrowser).Run(HMGE[env]);
            Log.Message("Navigating to " + HMGE[env])
            break;
        case "MAT":
            Browsers.Item(execBrowser).Run(MAT[env]);
            Log.Message("Navigating to " + MAT[env])
            break;
        case "ELT":
            Browsers.Item(execBrowser).Run(ELT[env]);
            Log.Message("Navigating to " + ELT[env])
            break;
        case "21st":
            Browsers.Item(execBrowser).Run(st[env]);
            Log.Message("Navigating to " + st[env])
            break;
        case "APR":
            Browsers.Item(execBrowser).Run(APR[env]);
            Log.Message("Navigating to " + APR[env])
            break;
        case "AMFM":
            Browsers.Item(execBrowser).Run(AMFM[env]);
            Log.Message("Navigating to " + AMFM[env])
            break;
        case "ELE":
            Browsers.Item(execBrowser).Run(ELE[env]);
            Log.Message("Navigating to " + ELE[env])
            break;
        default:
            Browsers.Item(execBrowser).Run(HS[env]);
            Log.Message("Navigating to " + HS[env])
    }
    //login to ols based on the parameter being passed 
    if (appLogin == "Y") {
        // if password is null then enrolling to ols based on the enrollment status   
        if (!enroll) {
            //enrolling to OLS    
            enrollOLS(pnumb)
        } else {
            // login to ols after resetting the password     
            loginOLS(pnumb, email)
        }
    }

}