﻿//USEUNIT ImportUnits

/*********************************************************************************************************************************************************************************
'
'     NAME                      :   GetXMLData  
'     AUTHOR                    :   Nivedita, Yash 
'     DATE                      :   3/22/2019
'     DESCRIPTION               :   This file consists of functions to read XML data
'
'*********************************************************************************************************************************************************************************/

//intialising variable for psnap and bsnap 
var psnap = {
    qa: QA_psnap,
    uat: UAT_psnap
}
var bsnap = {
    qa: QA_bsnap,
    uat: UAT_bsnap
}

//To get tag values from xml based on the tag mentioned 
//parameters - Policy number, xmlsource and other tag values which need to be fetched 

function TestXMLDOM(PolicyNumber, xmlsource, tag1, tag2, tag3, tag4) {
    try {
        //intailise variables  
        var Doc, s
        Doc = getActiveXObject("Msxml2.DOMDocument.6.0");
        Doc.async = false;
        //psnap url to fetch the data 
        Doc.load(xmlsource[env] + PolicyNumber + "/");
        // Report an error, if, for instance, the markup or file structure is invalid 
        if (Doc.parseError.errorCode != 0) {
            s = "Reason:\t" + Doc.parseError.reason + "\n" +
                "Line:\t" + aqConvert.VarToStr(Doc.parseError.line) + "\n" +
                "Pos:\t" + aqConvert.VarToStr(Doc.parseError.linePos) + "\n" +
                "Source:\t" + Doc.parseError.srcText;
            // Post an error to the log and exit
            Log.Error("Unable to fetch XML data for " + xmlsource[env], s);
            return;
        }
        //Fetch data for res1 from xml file
        var tag1value = Doc.getElementsByTagName(tag1)
        //Fetch data for res2 from xml file
        var tag2value = Doc.getElementsByTagName(tag2);
        //Fetch data for res3 from xml file
        var tag3value = Doc.getElementsByTagName(tag3)
        //Fetch data for res4 from xml file
        var tag4value = Doc.getElementsByTagName(tag4)

        var Result = []
        //return result with all the tag values 
        Result.push(tag1value.item(0).text, tag2value.item(0).text, tag3value.item(0).text, tag4value.item(0).text)
        Log.Message("XML data result for " + xmlsource[env] + " - " + Result)
        return Result
    } catch (e) {
        Log.Error("Unable to fetch XML data for " + xmlsource[env])
    }
}