﻿//USEUNIT ImportUnits

/*********************************************************************************************************************************************************************************
'
'     NAME                      :   Documents  
'     AUTHOR                    :   Nivedita, Yash 
'     DATE                      :   5/2/2019
'     DESCRIPTION               :   This file consists of common functions and objects for Documents and EOI for all users 
'
'*********************************************************************************************************************************************************************************/

//---------------------------EOI objects------------------------------------------

var btnAddemail = Aliases.Browser.pgEOI.btnAddemail
var btnAddfax =Aliases.Browser.pgEOI.btnAddfax
var btnPrintEOI = Aliases.Browser.pgEOI.btnPrintEOI
var btnSaveEOI = Aliases.Browser.pgEOI.btnSaveEOI
var btnSendEOI = Aliases.Browser.pgEOI.btnSendEOI
var headerEOI =  Aliases.Browser.pgEOI.headerPOI
var labelEOIConfirmMessage = Aliases.Browser.pgEOI.labelEOIConfirmMessage
var labelSendTo = Aliases.Browser.pgEOI.labelSendTo
var lnkCancel = Aliases.Browser.pgEOI.lnkCancel
var txtEOIEmail1 = Aliases.Browser.pgEOI.txtEOIEmail1
var txtEOIEmail2 = Aliases.Browser.pgEOI.txtEOIEmail2
var txtEOIEmail3 = Aliases.Browser.pgEOI.txtEOIEmail3
var txtEOIFax1 =Aliases.Browser.pgEOI.txtEOIFax1
var txtEOIFax2 =Aliases.Browser.pgEOI.txtEOIFax2
var txtEOIFax3 =Aliases.Browser.pgEOI.txtEOIFax3
var txtRecipient1 = Aliases.Browser.pgEOI.txtRecipient1
var txtRecipient2 = Aliases.Browser.pgEOI.txtRecipient2
var txtRecipient3 = Aliases.Browser.pgEOI.txtRecipient3
var txtSummary = Aliases.Browser.pgEOI.txtSummary
var chkbxSend = Aliases.Browser.pgEOI.chkbxSend
var btnOk = Aliases.Browser.Header_All.AlertBoxPOI.btnOk
var pnlAlertText = Aliases.Browser.Header_All.AlertBoxPOI.pnlAlertText
var pnlSummaryEmailrecipient1 = Aliases.Browser.pgEOI.pnlSummaryEmailrecipient1
var pnlSummaryFax1 = Aliases.Browser.pgEOI.pnlSummaryFax1

//---------------------------EOI functions------------------------------------------

var emailAddress1 = "testsummary@tester.com"
  var Recipient1 = "PaceQA"
  var EOIFax1 = "2342342345"
  var EOIFax1_1 = "234-2345"
  
  
//Validate the Header EOI shows correct form information  
 function validateHeaderEOI(polnumber)
 {
   //Get form type from HS Service Info DB table  
    var formType = getForm(polnumber)
   //Compare the formType with the header string 
   inString(headerEOI,formType)
 }
//Validate email address 
function validateEmailEOI(polnumber)
{
  validateEOIAlert(polnumber)
  //Get email address from HS Service Info DB table
  var emailPolicy = getEmail(polnumber)
  Log.Message("emailPolicy----"+emailPolicy)
  //Compare the email address in the SendTo 
  inStringSpecialChar(labelSendTo,emailPolicy)
}

function validateSummaryEOI(polnumber)
{
      
 validateEOIAlert(polnumber)
  
  dynamicWait(chkbxSend,5)
  //chkbxSend.ClickChecked()
  objectClick(labelSendTo)
  objectKeys(txtEOIEmail1,emailAddress1)
  objectKeys(txtRecipient1,Recipient1)
  objectKeys(txtEOIFax1,EOIFax1)
  var valEOIEmail = inString(pnlSummaryEmailrecipient1,emailAddress1)
  var Recipient = inString(pnlSummaryFax1,Recipient1)
  var EOIFax = inString(pnlSummaryFax1,EOIFax1)
  
}

function validateEOIAlert(polnumber)
{
  if (btnOk.Exists)
  {
    dynamicWait(pnlAlertText,8) 
    var phoneNumb = getBrandPhoneNumb(polnumber)
    Log.Checkpoint("Proof of insurance does not load validating partner phone number "+phoneNumb+" for policy "+polnumber)
    inString(pnlAlertText,phoneNumb)
    objectClick(btnOk)
  }
  
}

function validateNotificationEOI(polnumber)
{
  if (btnSendEOI.Exists)
  {
    objectClick(btnSendEOI)
    dynamicWait(labelEOIConfirmMessage,4)
    inStringSpecialChar(labelEOIConfirmMessage,emailAddress1)
    inString(labelEOIConfirmMessage,Recipient1)
    inString(labelEOIConfirmMessage,EOIFax1_1)
    var formType = getForm(polnumber)
    inString(labelEOIConfirmMessage,formType)
    
  }

}

function validatePartnerEmailAdressEOI(polnumber)
{
  emailAdress = getPartnerEmailAddres(polnumber)
  inString(labelEOIConfirmMessage,emailAdress)
}