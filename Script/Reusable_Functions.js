﻿//Logout  - need to update it based on 
function Log_Out()
{
    RowProp = new Array("idStr","ObjectIdentifier","ObjectType")
    RowVal = new Array("signOutButton","signOutButton","Link")
    var LogOut_Link = Sys.Browser("iexplore").Page("*").FindChild(RowProp, RowVal, 100)   
    if(LogOut_Link.Exists == true)
    {
      LogOut_Link.scrollIntoView(true)    
      aqUtils.Delay(3000)  
      Object_Click(LogOut_Link)
      Log.Checkpoint("Clicked on Logout.")
    }
  else
  {
    Log.Message("Logout link not found")
  }
}



function Logout_Application()
{
 Dynamic_Wait(lnkGoBackSU,3)
 Object_Click(lnkGoBackSU)
 aqUtils.Delay(3000)
 Browser_Close()
}


function Navigate_URL(URL)
{
  Browsers.Item(btIExplorer).Navigate(URL);
  Dynamic_Wait(Textbox_Userid,3)
  if (Textbox_Userid.Exists == true)
  {
   Log.Message("Application launched"); 
   return true;
  }
  else 
  {
    Log.Error("Couldn't launch the application");
    return false;
  } 
}